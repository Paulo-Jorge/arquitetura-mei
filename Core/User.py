#!/usr/bin/python
# -*- coding: UTF-8 -*-
from abc import ABCMeta, abstractmethod
from Core import Authentication
from Core import Profile

class User(object):
    __metaclass__ = ABCMeta
    @classmethod
    def checkAuth(self):
        pass

    @classmethod
    def editProfile(self):
        pass

    @classmethod
    def checkSession(self):
        pass

    @classmethod
    def __init__(self):
        self.___idUser = None
        """@AttributeType int"""
        self.___name = None
        """@AttributeType string"""
        self.___password = None
        """@AttributeType string"""
        self.___email = None
        """@AttributeType string"""
        self.___secureToken = None
        """@AttributeType string"""
        self.___userRole = None
        """@AttributeType string"""
        self._unnamed_Authentication_ = None
        # @AssociationType Core.Authentication
        # @AssociationMultiplicity 1
        self._unnamed_Profile_ = None
        # @AssociationType Core.Profile
        # @AssociationMultiplicity 1

